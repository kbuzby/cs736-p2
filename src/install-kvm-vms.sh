# Fetch updates and install necessary packages
sudo apt-get update
sudo apt-get install qemu-kvm libvirt-clients libvirt-daemon-system virt-manager

# Add yourself to the necessary groups
sudo adduser `whoami` libvirt
sudo adduser `whoami` libvirt-qemu

# Generate the default ssh key for connecting to guests
# Just leave defaults for the generation
ssh-keygen -t rsa

# Check out the setup script for creating the vm guests
mkdir -p contrib
git clone https://github.com/giovtorres/kvm-install-vm.git contrib/kvm-install-vm
pushd contrib/kvm-install-vm

# Finally, create the guests
sudo ./kvm-install-vm create -m 8192 -t ubuntu1804 ubuntu1
sudo ./kvm-install-vm create -m 8192 -t ubuntu1804 ubuntu2
sudo ./kvm-install-vm create -m 8192 -t ubuntu1804 ubuntu3
sudo ./kvm-install-vm create -m 8192 -t ubuntu1804 ubuntu4
popd
