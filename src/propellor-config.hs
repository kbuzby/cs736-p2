import Propellor
import Propellor.Property.DiskImage
import Propellor.Property.Parted
import qualified Propellor.Property.Apt as Apt
import qualified Propellor.Property.Chroot as Chroot
import qualified Propellor.Property.Grub as Grub
import qualified Propellor.Property.Hostname as Hostname
import qualified Propellor.Property.User as User

main :: IO ()
main = defaultMain hosts

-- The hosts propellor knows about.
hosts :: [Host]
hosts = [ mybox ]

-- An example host.
mybox :: Host
mybox = host "debian.home" $ props
  & imageBuilt (RawDiskImage "/tmp/test1.img") mychroot MSDOS
      [ partition EXT2 `mountedAt` "/boot"
       `setFlag` BootFlag
      , partition EXT4 `mountedAt` "/"
       `mountOpt` errorReadonly
       `addFreeSpace` MegaBytes 100 ]
  where mychroot d = Chroot.debootstrapped mempty d $ props
    & Hostname.setTo "test1"
  
    & osDebian Unstable X86_64
  
    & Apt.hasForeignArch "amd64"
    & Apt.stdArchiveLines
    & Apt.unattendedUpgrades
    & Apt.installed ["linux-image-amd64"]
    & Apt.installed ["etckeeper", "propellor", "ssh"]
    & Apt.installed ["qemu-kvm", "libvirt-clients",
                     "libvirt-daemon-system", "virt-manager"]
  
    & User.hasInsecurePassword (User "root") "root"
  
    & Grub.installed PC
    -- disable framebuffer so I ssh via qemu.
    & Grub.configured "GRUB_TERMINAL" "console"
    & Grub.cmdline_Linux_default "nofb bochs_drm.fbdev=off"
