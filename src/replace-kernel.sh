dhclient # get an IP if we don't have one already.
apt-get update
apt-get build-dep linux
apt-get install -y libncurses-dev libssl-dev
VERSION=4.19.1; export VERSION
wget -c https://cdn.kernel.org/pub/linux/kernel/v4.x/linux-$VERSION.tar.xz
tar xf linux-$VERSION.tar.xz
cp /boot/config-$(uname -r) linux-$VERSION/.config
make -k -j 4 -C linux-$VERSION menuconfig
make -k -j 4 -C linux-$VERSION # do not remove this line, it is necessary.
make -k -j 4 -C linux-$VERSION modules
make -k -j 4 -C linux-$VERSION install
make -k -j 4 -C linux-$VERSION modules_install
update-initramfs -c -k $VERSION
update-grub
